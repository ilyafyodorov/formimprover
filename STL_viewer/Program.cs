﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Text;

namespace STLViewer
{

    public partial class AppMainForm : Form
    {

        static class Proj
        {
            public static float[] projection { get; set; }
            public static double[] distorsion { get; set; }
            public static float minProjection = 0;
            public static float maxProjection = 0;
        }


        //Class for STL triangles
        public class Triangle
        {
            public double[] Normal = new double[3];
            public double[] Vertices = new double[9];
        }

        //Class for mesh edit points
        public class meshEditPoints
        {
            //point coordinates
            public double[] pointMorphingCoords = new double[3];
            //influence radius
            public double pointMorphingRadius;
            //deformation direction vector
            public double[] pointMorphingVectors = new double[3];
        }

        public void Morph(string InputSTLNameStr, string InputSettingsNameStr, string OutputSTLNameStr)
        {
            BinaryReader reader;
            BinaryWriter writer;

            char[] headArr = new char[80];
            int numTriangles;
            UInt16 spacer = 1;


            string inputSTL = InputSTLNameStr;
            string outputSTL = OutputSTLNameStr;
            string morphingInput = InputSettingsNameStr;

            //reading from the STL file
            this.txtConsole.AppendText("Reading input STL...");
            this.txtConsole.AppendText(Environment.NewLine);
            this.Refresh();
            try
            {
                reader = new BinaryReader(new FileStream(inputSTL, FileMode.Open));
            }
            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot open the input file.");
                return;
            }

            try
            {

                headArr = reader.ReadChars(80);
                numTriangles = reader.ReadInt32();

            }

            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot read STL header from file.");
                return;
            }

            Triangle[] Mesh = new Triangle[numTriangles];

            try
            {
                for (int i = 0; i < numTriangles; i++)
                {
                    Mesh[i] = new Triangle();
                    for (int j = 0; j < 3; j++)
                    {
                        Mesh[i].Normal[j] = reader.ReadSingle();
                    }
                    for (int j = 0; j < 9; j++)
                    {
                        Mesh[i].Vertices[j] = reader.ReadSingle();
                    }
                    reader.ReadBytes(2);
                }

                reader.Close();
            }

            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot read mesh from file.");
                return;
            }

            //read morphing settings from a text file

            this.txtConsole.AppendText("Reading morphing settings...");
            this.txtConsole.AppendText(Environment.NewLine);
            this.Refresh();
            
                string line;
            int numPoints;

            System.IO.StreamReader inpFile = new System.IO.StreamReader(@morphingInput);

            try
            {
                line = inpFile.ReadLine();
                numPoints = Int32.Parse(line);
            }
            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot read the number opf control points");
                return;
            }

            meshEditPoints[] controlPoints = new meshEditPoints[numPoints];
            for (int i = 0; i < numPoints; i++)
            {
                controlPoints[i] = new meshEditPoints();
            }

            try
            {
                for (int i = 0; i < numPoints; i++)
                {
                    line = inpFile.ReadLine();
                    string[] numbersStr = line.Split(',');
                    controlPoints[i].pointMorphingCoords[0] = double.Parse(numbersStr[0], CultureInfo.InvariantCulture.NumberFormat);
                    controlPoints[i].pointMorphingCoords[1] = double.Parse(numbersStr[1], CultureInfo.InvariantCulture.NumberFormat);
                    controlPoints[i].pointMorphingCoords[2] = double.Parse(numbersStr[2], CultureInfo.InvariantCulture.NumberFormat);
                }

                for (int i = 0; i < numPoints; i++)
                {
                    line = inpFile.ReadLine();
                    controlPoints[i].pointMorphingRadius = double.Parse(line, CultureInfo.InvariantCulture.NumberFormat);
                }

                for (int i = 0; i < numPoints; i++)
                {
                    line = inpFile.ReadLine();
                    string[] numbersStr = line.Split(',');
                    controlPoints[i].pointMorphingVectors[0] = double.Parse(numbersStr[0], CultureInfo.InvariantCulture.NumberFormat);
                    controlPoints[i].pointMorphingVectors[1] = double.Parse(numbersStr[1], CultureInfo.InvariantCulture.NumberFormat);
                    controlPoints[i].pointMorphingVectors[2] = double.Parse(numbersStr[2], CultureInfo.InvariantCulture.NumberFormat);
                }
            }

            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot read morphing settings file");
                return;
            }

            //Apply morphing

            double dist1, dist2, dist3;
            double impact1, impact2, impact3;

            Proj.distorsion = new double[numTriangles * 9];
            Proj.projection = new float[numTriangles * 3];

            for (int i = 0; i < numTriangles * 9; i++)
            {
                Proj.distorsion[i] = 0;
            }



            for (int j = 0; j < numPoints; j++)
            {
                //Console.WriteLine("Processing morphing point " + (j + 1) + " out of " + numPoints);

                this.txtConsole.AppendText("Processing morphing point " + (j + 1) + " out of " + numPoints);
                this.txtConsole.AppendText(Environment.NewLine);
                this.Refresh();
                Application.DoEvents();

                for (int i = 0; i < numTriangles; i++)
                {

                    //Calculate distance from current point to each of 3 vertices
                    dist1 = Math.Pow(Mesh[i].Vertices[0] - controlPoints[j].pointMorphingCoords[0], 2);
                    dist1 = dist1 + Math.Pow(Mesh[i].Vertices[1] - controlPoints[j].pointMorphingCoords[1], 2);
                    dist1 = dist1 + Math.Pow(Mesh[i].Vertices[2] - controlPoints[j].pointMorphingCoords[2], 2);
                    dist1 = Math.Pow(dist1, 0.5);

                    dist2 = Math.Pow(Mesh[i].Vertices[3] - controlPoints[j].pointMorphingCoords[0], 2);
                    dist2 = dist2 + Math.Pow(Mesh[i].Vertices[4] - controlPoints[j].pointMorphingCoords[1], 2);
                    dist2 = dist2 + Math.Pow(Mesh[i].Vertices[5] - controlPoints[j].pointMorphingCoords[2], 2);
                    dist2 = Math.Pow(dist2, 0.5);

                    dist3 = Math.Pow(Mesh[i].Vertices[6] - controlPoints[j].pointMorphingCoords[0], 2);
                    dist3 = dist3 + Math.Pow(Mesh[i].Vertices[7] - controlPoints[j].pointMorphingCoords[1], 2);
                    dist3 = dist3 + Math.Pow(Mesh[i].Vertices[8] - controlPoints[j].pointMorphingCoords[2], 2);
                    dist3 = Math.Pow(dist3, 0.5);

                    //Calculate impact factor for every vertex
                    if (dist1 < controlPoints[j].pointMorphingRadius)
                    {
                        impact1 = 1.0 - (dist1 / controlPoints[j].pointMorphingRadius);
                    }
                    else
                    {
                        impact1 = 0.0;
                    }

                    if (dist2 < controlPoints[j].pointMorphingRadius)
                    {
                        impact2 = 1.0 - (dist2 / controlPoints[j].pointMorphingRadius);
                    }
                    else
                    {
                        impact2 = 0.0;
                    }

                    if (dist3 < controlPoints[j].pointMorphingRadius)
                    {
                        impact3 = 1.0 - (dist3 / controlPoints[j].pointMorphingRadius);
                    }
                    else
                    {
                        impact3 = 0.0;
                    }

                    //Apply transformation to each vertex

                    Mesh[i].Vertices[0] = Mesh[i].Vertices[0] + controlPoints[j].pointMorphingVectors[0] * impact1 * impact1;
                    Mesh[i].Vertices[1] = Mesh[i].Vertices[1] + controlPoints[j].pointMorphingVectors[1] * impact1 * impact1;
                    Mesh[i].Vertices[2] = Mesh[i].Vertices[2] + controlPoints[j].pointMorphingVectors[2] * impact1 * impact1;

                    Mesh[i].Vertices[3] = Mesh[i].Vertices[3] + controlPoints[j].pointMorphingVectors[0] * impact2 * impact2;
                    Mesh[i].Vertices[4] = Mesh[i].Vertices[4] + controlPoints[j].pointMorphingVectors[1] * impact2 * impact2;
                    Mesh[i].Vertices[5] = Mesh[i].Vertices[5] + controlPoints[j].pointMorphingVectors[2] * impact2 * impact2;

                    Mesh[i].Vertices[6] = Mesh[i].Vertices[6] + controlPoints[j].pointMorphingVectors[0] * impact3 * impact3;
                    Mesh[i].Vertices[7] = Mesh[i].Vertices[7] + controlPoints[j].pointMorphingVectors[1] * impact3 * impact3;
                    Mesh[i].Vertices[8] = Mesh[i].Vertices[8] + controlPoints[j].pointMorphingVectors[2] * impact3 * impact3;

                    Proj.distorsion[i * 9 + 0] = Proj.distorsion[i * 9 + 0] + controlPoints[j].pointMorphingVectors[0] * impact1 * impact1;
                    Proj.distorsion[i * 9 + 1] = Proj.distorsion[i * 9 + 1] + controlPoints[j].pointMorphingVectors[1] * impact1 * impact1;
                    Proj.distorsion[i * 9 + 2] = Proj.distorsion[i * 9 + 2] + controlPoints[j].pointMorphingVectors[2] * impact1 * impact1;

                    Proj.distorsion[i * 9 + 3] = Proj.distorsion[i * 9 + 3] + controlPoints[j].pointMorphingVectors[0] * impact2 * impact2;
                    Proj.distorsion[i * 9 + 4] = Proj.distorsion[i * 9 + 4] + controlPoints[j].pointMorphingVectors[1] * impact2 * impact2;
                    Proj.distorsion[i * 9 + 5] = Proj.distorsion[i * 9 + 5] + controlPoints[j].pointMorphingVectors[2] * impact2 * impact2;

                    Proj.distorsion[i * 9 + 6] = Proj.distorsion[i * 9 + 6] + controlPoints[j].pointMorphingVectors[0] * impact3 * impact3;
                    Proj.distorsion[i * 9 + 7] = Proj.distorsion[i * 9 + 7] + controlPoints[j].pointMorphingVectors[1] * impact3 * impact3;
                    Proj.distorsion[i * 9 + 8] = Proj.distorsion[i * 9 + 8] + controlPoints[j].pointMorphingVectors[2] * impact3 * impact3;


                }
            }

            for (int i = 0; i < numTriangles; i++)
            {
                Proj.projection[i * 3 + 0] = Convert.ToSingle(Proj.distorsion[i * 9 + 0] * Mesh[i].Normal[0] + Proj.distorsion[i * 9 + 1] * Mesh[i].Normal[1] + Proj.distorsion[i * 9 + 2] * Mesh[i].Normal[2]);
                Proj.projection[i * 3 + 1] = Convert.ToSingle(Proj.distorsion[i * 9 + 3] * Mesh[i].Normal[0] + Proj.distorsion[i * 9 + 4] * Mesh[i].Normal[1] + Proj.distorsion[i * 9 + 5] * Mesh[i].Normal[2]);
                Proj.projection[i * 3 + 2] = Convert.ToSingle(Proj.distorsion[i * 9 + 6] * Mesh[i].Normal[0] + Proj.distorsion[i * 9 + 7] * Mesh[i].Normal[1] + Proj.distorsion[i * 9 + 8] * Mesh[i].Normal[2]);
            }

            for (int i = 0; i < numTriangles * 3; i++)
            {
                if (Proj.projection[i] < Proj.minProjection) Proj.minProjection = Proj.projection[i];
                if (Proj.projection[i] > Proj.maxProjection) Proj.maxProjection = Proj.projection[i];
            }



            //writing the STL file
            this.txtConsole.AppendText("Writing output STL...");
            this.txtConsole.AppendText(Environment.NewLine);
            this.Refresh();

            try
            {
                writer = new BinaryWriter(new FileStream(outputSTL, FileMode.Create));
            }
            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot open the output file for writing");
                return;
            }


            try
            {
                writer.Write(headArr);
                writer.Write(numTriangles);
                for (int i = 0; i < numTriangles; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        writer.Write(Convert.ToSingle(Mesh[i].Normal[j]));
                    }
                    for (int j = 0; j < 9; j++)
                    {
                        writer.Write(Convert.ToSingle(Mesh[i].Vertices[j]));
                    }
                    writer.Write(spacer);
                }
                writer.Close();
            }
            catch (IOException e)
            {
                this.txtConsole.AppendText(e.Message + "\n Cannot write to file.");
                return;
            }

            Mesh = null;
            Proj.distorsion = null;

            this.txtConsole.AppendText("Morphing completed!");
            this.txtConsole.AppendText(Environment.NewLine);

        }
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AppMainForm());
        }

        
    }
}
