﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using STL_Tools;
using OpenTK.Graphics.OpenGL;
using BatuGL;
using Mouse_Orbit;

namespace STLViewer
{
    public partial class AppMainForm : Form
    {
        bool monitorLoaded = false;
        bool moveForm = false;
        int moveOffsetX = 0;
        int moveOffsetY = 0;
        Batu_GL.VAO_TRIANGLES modelVAO = null; // 3d model vertex array object
        private Orbiter orb;
        Vector3 minPos = new Vector3();
        Vector3 maxPos = new Vector3();

        public AppMainForm()
        {
            /* dot/comma selection for floating point numbers */
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            InitializeComponent();
            orb = new Orbiter();
            GL_Monitor.MouseDown += orb.Control_MouseDownEvent;
            GL_Monitor.MouseUp += orb.Control_MouseUpEvent;
            GL_Monitor.MouseWheel += orb.Control_MouseWheelEvent;
            GL_Monitor.KeyPress += orb.Control_KeyPress_Event;
        }

        private void DrawTimer_Tick(object sender, EventArgs e)
        {
            orb.UpdateOrbiter(MousePosition.X, MousePosition.Y);
            GL_Monitor.Invalidate();
            if (moveForm)
            {
                this.SetDesktopLocation(MousePosition.X - moveOffsetX, MousePosition.Y - moveOffsetY);
            }
        }

        private void GL_Monitor_Load(object sender, EventArgs e)
        {
            GL_Monitor.AllowDrop = true;
            monitorLoaded = true;
            GL.ClearColor(Color.Gray);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Batu_GL.Configure(GL_Monitor, Batu_GL.Ortho_Mode.CENTER);
        }
        private void OpenSTL_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                InputSTLName.Text = openFileDialog1.FileName;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void OpenSettings_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog2 = new OpenFileDialog();

            if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                InputSettingsName.Text = openFileDialog2.FileName;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            pictureBox1.Show();
            this.Refresh();

            /* deinitialize the gl monitor to clear the screen */
            modelVAO = null;
            Batu_GL.Configure(GL_Monitor, Batu_GL.Ortho_Mode.CENTER);
            GL_Monitor.SwapBuffers();
            MinMaxBox.Text = "";
            Proj.projection = null;
            Proj.minProjection = 0;
            Proj.maxProjection = 0;

            string InputSTLNameStr, InputSettingsNameStr, OutputSTLNameStr;
            InputSTLNameStr = InputSTLName.Text;
            InputSettingsNameStr = InputSettingsName.Text;
            OutputSTLNameStr = OutputSTLName.Text;
            Morph(InputSTLNameStr, InputSettingsNameStr, OutputSTLNameStr);
            this.txtConsole.AppendText("Displaying morphing result...");
            this.txtConsole.AppendText(Environment.NewLine);
            this.Refresh();
            Application.DoEvents();
            ReadSelectedFile(OutputSTLName.Text);
            pictureBox1.Hide();
            this.txtConsole.AppendText("Done!");
            this.txtConsole.AppendText(Environment.NewLine);
            this.Refresh();
        }

        private void txtConsole_TextChanged(object sender, EventArgs e)
        {
            txtConsole.SelectionStart = txtConsole.Text.Length;
            txtConsole.ScrollToCaret();
        }

        private void ConfigureBasicLighting(Color modelColor)
        {
            float LightDistMin = Convert.ToSingle(Math.Sqrt(minPos.x * minPos.x + minPos.y * minPos.y + minPos.z * minPos.z));
            float LightDistMax = Convert.ToSingle(Math.Sqrt(maxPos.x * maxPos.x + maxPos.y * maxPos.y + maxPos.z * maxPos.z));
            float LightDist = (LightDistMin + LightDistMax) * 5;

            float[] light_1 = new float[] {
            0.2f * modelColor.R / 255.0f,
            0.2f * modelColor.G / 255.0f,
            0.2f * modelColor.B / 255.0f,
            0.2f };
            float[] light_2 = new float[] {
            10f * modelColor.R / 255.0f,
            10f * modelColor.G / 255.0f,
            10f * modelColor.B / 255.0f,
            10.0f };
            float[] specref = new float[] { 
                0.8f * modelColor.R / 255.0f, 
                0.8f * modelColor.G / 255.0f, 
                0.8f * modelColor.B / 255.0f, 
                1.0f };
            float[] specular_0 = new float[] { -1.0f, -1.0f, -1.0f, 1.0f };
            float[] specular_1 = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };
            float[] lightPos_0 = new float[] { LightDist, LightDist, LightDist, 1.0f };
            float[] lightPos_1 = new float[] { -LightDist, -LightDist, -LightDist, 1.0f };

            GL.Enable(EnableCap.Lighting);
            /* light 0 */
            GL.Light(LightName.Light0, LightParameter.Ambient, light_1);
            GL.Light(LightName.Light0, LightParameter.Diffuse, light_2);
            GL.Light(LightName.Light0, LightParameter.Specular, specular_0);
            GL.Light(LightName.Light0, LightParameter.Position, lightPos_0);
            GL.Enable(EnableCap.Light0);
            /* light 1 */
            GL.Light(LightName.Light1, LightParameter.Ambient, light_1);
            GL.Light(LightName.Light1, LightParameter.Diffuse, light_2);
            GL.Light(LightName.Light1, LightParameter.Specular, specular_1);
            GL.Light(LightName.Light1, LightParameter.Position, lightPos_1);
            GL.Enable(EnableCap.Light1);
            /*material settings  */
            GL.Enable(EnableCap.ColorMaterial);
            GL.ColorMaterial(MaterialFace.Front, ColorMaterialParameter.AmbientAndDiffuse);
            GL.Material(MaterialFace.Front, MaterialParameter.AmbientAndDiffuse, light_1);
            GL.Material(MaterialFace.Front, MaterialParameter.Shininess, 5);
            GL.Enable(EnableCap.Normalize);
        }

        private void GL_Monitor_Paint(object sender, PaintEventArgs e)
        {
            if (!monitorLoaded)
                return;

            Batu_GL.Configure(GL_Monitor, Batu_GL.Ortho_Mode.CENTER);
            if (modelVAO != null) ConfigureBasicLighting(modelVAO.color);
            GL.Translate(orb.PanX, orb.PanY, 0);
            GL.Rotate(orb.orbitStr.angle, orb.orbitStr.ox, orb.orbitStr.oy, orb.orbitStr.oz);
            GL.Scale(orb.scaleVal, orb.scaleVal, orb.scaleVal);
            GL.Translate(-minPos.x, -minPos.y, -minPos.z);
            GL.Translate(-(maxPos.x - minPos.x) / 2.0f, -(maxPos.y - minPos.y) / 2.0f, -(maxPos.z - minPos.z) / 2.0f);
            if (modelVAO != null) modelVAO.Draw();

            GL_Monitor.SwapBuffers();
        }

        private void ReadSelectedFile(string fileName)
        {
            STLReader stlReader = new STLReader(fileName);
            TriangleMesh[] meshArray = stlReader.ReadFile();

            modelVAO = new Batu_GL.VAO_TRIANGLES();
            modelVAO.parameterArray = STLExport.Get_Mesh_Vertices(meshArray);
            modelVAO.normalArray = STLExport.Get_Mesh_Normals(meshArray);

            //modelVAO.color = Color.Crimson;

            minPos = stlReader.GetMinMeshPosition(meshArray);
            maxPos = stlReader.GetMaxMeshPosition(meshArray);

            MinMaxBox.Text = "Min:" + Proj.minProjection + " ; Max:" + Proj.maxProjection;

            float[] colorArrayData = new float[modelVAO.parameterArray.Length];
            for (int k = 0; k < modelVAO.parameterArray.Length / 3; k++)
            {
                float Aver = (Proj.minProjection + Proj.maxProjection) / 2;
                if (Proj.projection[k]>= Aver)
                {
                    colorArrayData[k * 3] = 1.0f * (Proj.projection[k]-Aver) / (Proj.maxProjection - Aver);
                    colorArrayData[k * 3 + 1] = 1.0f * (1 - (Proj.projection[k] - Aver) / (Proj.maxProjection - Aver));
                    colorArrayData[k * 3 + 2] = 0.0f;
                }
                else
                {
                    colorArrayData[k * 3] = 0.0f;
                    colorArrayData[k * 3 + 1] = 1.0f * (1 - (-Proj.projection[k] + Aver) / (Aver - Proj.minProjection));
                    colorArrayData[k * 3 + 2] = 1.0f * (-Proj.projection[k] + Aver) / (Aver - Proj.minProjection);
                }
            }

            modelVAO.colorArray = colorArrayData;

            orb.Reset_Orientation();
            orb.Reset_Pan();
            orb.Reset_Scale();
            if (stlReader.Get_Process_Error())
            { 
                modelVAO = null;
                /* if there is an error, deinitialize the gl monitor to clear the screen */
                Batu_GL.Configure(GL_Monitor, Batu_GL.Ortho_Mode.CENTER);
                GL_Monitor.SwapBuffers();
            }

            meshArray = null;
        }

        private void FileMenuImportBt_Click(object sender, EventArgs e)
        {
            OpenFileDialog newFileDialog = new OpenFileDialog();
            newFileDialog.Filter = "STL Files|*.stl;*.txt;";

            if (newFileDialog.ShowDialog() == DialogResult.OK)
            {
                ReadSelectedFile(newFileDialog.FileName);
            }
        }

        private void FileMenuExitBt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CloseBt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MinimizeBt_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void AppToolBarMStp_MouseDown(object sender, MouseEventArgs e)
        {
            moveForm = true;
            moveOffsetX = MousePosition.X - this.Location.X;
            moveOffsetY = MousePosition.Y - this.Location.Y;
        }

        private void AppToolBarMStp_MouseUp(object sender, MouseEventArgs e)
        {
            moveForm = false;
            moveOffsetX = 0;
            moveOffsetY = 0;
        }

        private void AppToolBarMStp_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Normal;
            else this.WindowState = FormWindowState.Maximized;
        }

        private void AppTitleLb_MouseDown(object sender, MouseEventArgs e)
        {
            moveForm = true;
            moveOffsetX = MousePosition.X - this.Location.X;
            moveOffsetY = MousePosition.Y - this.Location.Y;
        }

        private void AppTitleLb_MouseUp(object sender, MouseEventArgs e)
        {
            moveForm = false;
            moveOffsetX = 0;
            moveOffsetY = 0;
        }

        private void AppTitleLb_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Normal;
            else this.WindowState = FormWindowState.Maximized;
        }

        private void MaximizeBt_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized) this.WindowState = FormWindowState.Normal;
            else this.WindowState = FormWindowState.Maximized;
        }

        private void GL_Monitor_DragDrop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData(DataFormats.FileDrop);
            if (data != null)
            {
                string[] fileNames = data as string[];
                string ext = System.IO.Path.GetExtension(fileNames[0]);
                if (fileNames.Length > 0 && (ext == ".stl" || ext == ".STL" || ext == ".txt" || ext == ".TXT"))
                {
                    ReadSelectedFile(fileNames[0]);
                }
            }
        }

        private void GL_Monitor_DragEnter(object sender, DragEventArgs e)
        {
            // if the extension is not *.txt or *.stl change drag drop effect symbol
            var data = e.Data.GetData(DataFormats.FileDrop);
            if (data != null)
            {
                string[] fileNames = data as string[];
                string ext = System.IO.Path.GetExtension(fileNames[0]);
                if (ext == ".stl" || ext == ".STL" || ext == ".txt" || ext == ".TXT") 
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }                
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var fileToOpen = InputSettingsName.Text;
            var process = new Process();

            Process.Start(@"notepad.exe", fileToOpen);
            //process.WaitForExit();
        }
    }
}