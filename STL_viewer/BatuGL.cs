﻿using System.Drawing;
using OpenTK.Graphics.OpenGL;

namespace BatuGL
{
    public static class Batu_GL
    {
        public enum Ortho_Mode { CENTER, BLEFT };

        public static void Configure(OpenTK.GLControl refGLControl, Ortho_Mode ortho)
        {
            GL.ClearColor(Color.White);
            refGLControl.VSync = false;
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.Zero);

            GL.ClearColor(Color.White);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.Viewport(refGLControl.Size);
            if(ortho == Ortho_Mode.CENTER)
            {
                GL.Ortho(-refGLControl.Width / 2, refGLControl.Width / 2, -refGLControl.Height / 2, refGLControl.Height / 2, -20000, +20000);
            }
            else
            {
                GL.Ortho(0, refGLControl.Width, 0, refGLControl.Height, -20000, +20000);
            }
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearDepth(1.0f);
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            GL.Hint(HintTarget.PerspectiveCorrectionHint, HintMode.Nicest);

        }

        public static void Draw_WCS(float size = 1000.0f)
        {
            /* WCS for Debug X+ = Red, Y+ = Green, Z+ = Blue */
            float wcsSize = 1000.0f;
            GL.LineWidth(5.0f);
            GL.Begin(PrimitiveType.Lines);
            GL.Color3(1.0f, 0.0f, 0.0f);
            GL.Vertex3(0.0f, 0.0f, 0.0f);
            GL.Vertex3(wcsSize, 0.0f, 0.0f);
            GL.Color3(0.0f, 1.0f, 0.0f);
            GL.Vertex3(0.0f, 0.0f, 0.0f);
            GL.Vertex3(0.0f, wcsSize, 0.0f);
            GL.Color3(0.0f, 0.0f, 1.0f);
            GL.Vertex3(0.0f, 0.0f, 0.0f);
            GL.Vertex3(0.0f, 0.0f, wcsSize);
            GL.End();
        }

        public class VAO_TRIANGLES
        {
            public Color color { get; set; }
            public float[] scale { get; set; }
            public float[] parameterArray { get; set; }
            public float[] normalArray { get; set; }
            public float[] colorArray { get; set; }

            public VAO_TRIANGLES()
            {
                color = Color.LightGray;
                scale = new float[3] { 1.0f, 1.0f, 1.0f };
            }

            public void Draw()
            {
                GL.PushMatrix();
                GL.Scale(scale[0], scale[1], scale[2]);
                GL.EnableClientState(ArrayCap.VertexArray);
                GL.EnableClientState(ArrayCap.NormalArray);
                GL.EnableClientState(ArrayCap.ColorArray);
                GL.VertexPointer(3, VertexPointerType.Float, 0, parameterArray);
                GL.ColorPointer(3, ColorPointerType.Float, 0, colorArray);
                GL.NormalPointer(NormalPointerType.Float, 0, normalArray);       
                GL.DrawArrays(PrimitiveType.Triangles, 0, parameterArray.Length / 3);
                GL.DisableClientState(ArrayCap.NormalArray);
                GL.DisableClientState(ArrayCap.VertexArray);
                GL.DisableClientState(ArrayCap.ColorArray);
                GL.PopMatrix();
            }
        }
    }
}
